﻿using System;
namespace CueCardBackend.Classes
{
    public abstract class Card
    {
        protected string _question;
        protected string _answer;
        public Card(string question, string answer)
        {
            _question = question;
            _answer = answer;
        }

        public string answer
        {
            get { return _answer; }
            set { _answer = value; }
        }

        public string question
        {
            get { return _question; }
            set { _question = value; }
        }


    }
}
