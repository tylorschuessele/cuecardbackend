﻿using System;
namespace CueCardBackend.Classes
{
    public class Group
    {
        protected string _name;
        protected Set[] _sets;
        protected User[] _members;
        protected User _owner;

        public Group(string n, User o)
        {
            _name = n;
            _owner = o;

        }

        public Group(string n, User o, Set[] s)
        {
            _name = n;
            _owner = o;
            _sets = s;
        }

        public Group(string n, User o, User[] m)
        {
            _name = n;
            _owner = o;
            _members = m;

        }

        public Group(string n, User o, User[] m, Set[] s)
        {
            _name = n;
            _owner = o;
            _members = m;
            _sets = s;
        }

        public void addSet(Set s)
        {
            Set[] newSets = new Set[(_sets.Length + 1)];
            newSets[0] = s;
            for (int i = 1; i < (_sets.Length); i++)
            {
                newSets[i] = _sets[i];
            }
            _sets = newSets;
        }


    }
}
