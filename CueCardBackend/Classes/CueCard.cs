﻿using System;
namespace CueCardBackend.Classes
{
    public class CueCard : Card
    {
        
        public CueCard(string question, string answer) : base(question, answer)
        {
            this._question = question;
            this._answer = answer;

        }

    }
}
