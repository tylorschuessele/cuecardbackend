﻿using System;
namespace CueCardBackend.Classes
{
    public abstract class QuizCard : Card
    {
        public const int MAX_CHOICES = 4;

        protected string[] _choices;

        public QuizCard(string question, string answer) : base(question, answer)
        {
            _question = question;
            _answer = answer;

        }

        public QuizCard(string question, string answer, string[] choices) : base(question, answer)
        {
            _question = question;
            _answer = answer;
            _choices = choices;

        }

        public string GetChoice(int index)
        {
            if((index + 1) <= _choices.Length)
            {
                return _choices[index];
            }
            return "";
        }

        public void AddChoice(string choice)
        {
            if(_choices.Length + 1 > MAX_CHOICES)
            {
                return;
            }

            string[] newChoices = new string[_choices.Length + 1];
            for(int i = 0; i < _choices.Length; i++)
            {
                newChoices[i] = _choices[i];
            }
            newChoices[_choices.Length + 1] = choice;
            _choices = newChoices;
        }

        public void AddChoices(string[] c)
        {
            if (_choices.Length >= MAX_CHOICES)
            {
                return;
            }

            string[] newChoices;

            if (_choices.Length + c.Length <= MAX_CHOICES)
            {
                newChoices = new string[_choices.Length + c.Length];
            }
            else
            {
                newChoices = new string[MAX_CHOICES];
            }

            for (int i = 0; i < _choices.Length; i++)
            {
                newChoices[i] = _choices[i];
            }

            for (int i = (_choices.Length); i < newChoices.Length; i++)
            {
                newChoices[i] = c[ (i - _choices.Length)];
            }
            
            _choices = newChoices;
        }

        public void EditChoice(int index, string c)
        {
            if(index > MAX_CHOICES)
            {
                return;
            }

            _choices[index] = c;


        }

        public void RemoveAllChoices()
        {
            _choices = new string[0];
        }

        public void RemoveChoice(int index)
        {
            if ((index > MAX_CHOICES) || (_choices.Length < index + 1) )
            {
                return;
            }

            string[] newChoices = new string[_choices.Length - 1];

            for(int i = 0; i <= index; i++)
            {
                newChoices[i] = _choices[i];
            }

            for(int i = index; i < newChoices.Length; i++)
            {
                newChoices[i] = _choices[i + 1];
            }


        }
    }
}
