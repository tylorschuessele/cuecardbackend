﻿using System;
namespace CueCardBackend.Classes
{
    public class Set
    {
        protected Card[] _cards;
        protected string _name;

        public Set(Card[] cards, string name)
        {
            _name = name;
            _cards = cards;
        }

        public Card[] getCards()
        {
            return _cards;
        }

        public Card getCard(int cardNumber)
        {
            if(cardNumber >= 0 &&  cardNumber <= _cards.Length - 1)
            {
                return _cards[cardNumber];
            }
            else
            {
                return new CueCard("Invalid Card", "");
            }
            
        }

        public void addCard(Card c, String type)
        {
            if (type == "cue")
            {
                CueCard[] newSet = new CueCard[(_cards.Length + 1)];
                for (int i = 0; i < (_cards.Length); i++)
                {
                    newSet[i] = (CueCard)_cards[i];
                }
                newSet[(_cards.Length + 1)] = (CueCard)c;
                _cards = newSet;
            }
            else if (type == "quiz")
            {
                QuizCard[] newSet = new QuizCard[(_cards.Length + 1)];
                for (int i = 0; i < (_cards.Length); i++)
                {
                    newSet[i] = (QuizCard)_cards[i];
                }
                newSet[(_cards.Length + 1)] = (QuizCard)c;
                _cards = newSet;
            }
        
        }

        public void removeCard(Card c)
        {
            if(_cards.Length == 0)
            {
                return;
            }
            Card[] newSet = new Card[(_cards.Length - 1)];

            int newArrayIndex = 0;
            for (int i = 0; i < _cards.Length; i++)
            {
                if (!(_cards[i].Equals(c)))
                {
                    newSet[newArrayIndex] = _cards[i];
                    newArrayIndex++;
                }
            }
            

        }



    }
}
